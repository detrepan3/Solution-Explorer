﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="16008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Framework" Type="Folder">
			<Item Name="Compressed Items.lvlibp" Type="LVLibp" URL="../../builds/PPLs/Compressed Items.lvlibp">
				<Item Name="Common" Type="Folder">
					<Item Name="Compress Step.lvclass" Type="LVClass" URL="../../builds/PPLs/Compressed Items.lvlibp/Compress Step/Compress Step.lvclass"/>
					<Item Name="Compressed File.lvclass" Type="LVClass" URL="../../builds/PPLs/Compressed Items.lvlibp/Compressed File/Compressed File.lvclass"/>
					<Item Name="Compressed Item Config.lvclass" Type="LVClass" URL="../../builds/PPLs/Compressed Items.lvlibp/Compressed Item Config/Compressed Item Config.lvclass"/>
				</Item>
				<Item Name="SFX" Type="Folder">
					<Item Name="Build SFX.lvclass" Type="LVClass" URL="../../builds/PPLs/Compressed Items.lvlibp/Build SFX/Build SFX.lvclass"/>
					<Item Name="SFX.lvclass" Type="LVClass" URL="../../builds/PPLs/Compressed Items.lvlibp/SFX/SFX/SFX.lvclass"/>
				</Item>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Delete Elements from 2D Array (String)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (String)__ogtk.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="File Exists - Scalar__ogtk.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/user.lib/_OpenG.lib/file/file.llb/File Exists - Scalar__ogtk.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="LVRowAndColumnUnsignedTypeDef.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRowAndColumnUnsignedTypeDef.ctl"/>
				<Item Name="MGI Change Detector (T).vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/user.lib/_MGI/Application Control/MGI Change Detector/MGI Change Detector (T).vi"/>
				<Item Name="MGI Gray if Empty String.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/user.lib/_MGI/Application Control/MGI Gray if/MGI Gray if Empty String.vi"/>
				<Item Name="MGI Gray if False.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/user.lib/_MGI/Application Control/MGI Gray if/MGI Gray if False.vi"/>
				<Item Name="MGI Gray if True.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/user.lib/_MGI/Application Control/MGI Gray if/MGI Gray if True.vi"/>
				<Item Name="MGI Open Explorer Window.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/user.lib/_MGI/File/MGI Open Explorer Window.vi"/>
				<Item Name="MGI Wrap String to Pixel Width.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/user.lib/_MGI/Picture &amp; Image/MGI Wrap String to Pixel Width.vi"/>
				<Item Name="MGI Wrap Word to Pixel Width.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/user.lib/_MGI/Picture &amp; Image/MGI Wrap Word to Pixel Width.vi"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Reorder 2D Array2 (String)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Reorder 2D Array2 (String)__ogtk.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Sort 1D Array (I32)__ogtk.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="../../builds/PPLs/Compressed Items.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Ionic.Zip.dll" Type="Document" URL="../../builds/PPLs/Ionic.Zip.dll"/>
			<Item Name="Project.lvlibp" Type="LVLibp" URL="../../builds/PPLs/Project.lvlibp">
				<Item Name="Helpers" Type="Folder">
					<Item Name="Build Steps" Type="Folder">
						<Item Name="Build Spec.lvclass" Type="LVClass" URL="../../builds/PPLs/Project.lvlibp/Build Spec/Build Spec.lvclass"/>
						<Item Name="Checksum.ctl" Type="VI" URL="../../builds/PPLs/Project.lvlibp/Project/Checksum.ctl"/>
						<Item Name="Manage Project.lvclass" Type="LVClass" URL="../../builds/PPLs/Project.lvlibp/Build Steps/Manage Project/Manage Project.lvclass"/>
					</Item>
					<Item Name="Binary File.lvclass" Type="LVClass" URL="../../builds/PPLs/Project.lvlibp/Binary File/Binary File.lvclass"/>
					<Item Name="File List.lvclass" Type="LVClass" URL="../../builds/PPLs/Project.lvlibp/File List/File List.lvclass"/>
				</Item>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="AB_Generate_Error_Cluster.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/AppBuilder/AB_Generate_Error_Cluster.vi"/>
				<Item Name="AB_Relative_Path_Type.ctl" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/AppBuilder/AB_Relative_Path_Type.ctl"/>
				<Item Name="AB_UI_Change_Path_from_Label.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/resource/Framework/Providers/Builds/AppBuilder/AB_UI_Change_Path_from_Label.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape XML.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/xml.llb/Escape XML.vi"/>
				<Item Name="EscapeChars.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/xml.llb/EscapeChars.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="File Exists - Scalar__ogtk.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/user.lib/_OpenG.lib/file/file.llb/File Exists - Scalar__ogtk.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GetTargetBuildSpecs (project reference).vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/AppBuilder/GetTargetBuildSpecs (project reference).vi"/>
				<Item Name="GetTargetBuildSpecs.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/AppBuilder/GetTargetBuildSpecs.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Invoke BuildTarget.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/AppBuilder/Invoke BuildTarget.vi"/>
				<Item Name="Is Name Multiplatform.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Is Name Multiplatform.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MD5Checksum core.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/MD5Checksum.llb/MD5Checksum core.vi"/>
				<Item Name="MD5Checksum File.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/MD5Checksum.llb/MD5Checksum File.vi"/>
				<Item Name="MD5Checksum format message-digest.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/MD5Checksum.llb/MD5Checksum format message-digest.vi"/>
				<Item Name="MD5Checksum pad.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/MD5Checksum.llb/MD5Checksum pad.vi"/>
				<Item Name="MD5Checksum string.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/MD5Checksum.llb/MD5Checksum string.vi"/>
				<Item Name="MGI Get Tree Tag Children.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/user.lib/_MGI/Tree/MGI Get Tree Tag Children.vi"/>
				<Item Name="MGI Open Explorer Window.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/user.lib/_MGI/File/MGI Open Explorer Window.vi"/>
				<Item Name="MGI Replace File Extension.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/user.lib/_MGI/File/MGI Replace File Extension.vi"/>
				<Item Name="NI_App_Builder_API.lvlib" Type="Library" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/AppBuilder/AB_API_Simple/NI_App_Builder_API.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_XML.lvlib" Type="Library" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/xml/NI_XML.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Project.lvclass" Type="LVClass" URL="../../builds/PPLs/Project.lvlibp/Project/Project.lvclass"/>
				<Item Name="provcom_CheckForInvalidCharacters.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/resource/Framework/Providers/Common/provcom_CheckForInvalidCharacters.vi"/>
				<Item Name="provcom_GetTargetOS.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/resource/Framework/Providers/Common/provcom_GetTargetOS.vi"/>
				<Item Name="provcom_StringGlobals.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/resource/Framework/Providers/Common/provcom_StringGlobals.vi"/>
				<Item Name="provcom_Utility_IsEmptyOrWhiteSpace.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/resource/Framework/Providers/Common/provcom_Utility_IsEmptyOrWhiteSpace.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="../../builds/PPLs/Project.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Run VI.lvlibp" Type="LVLibp" URL="../../builds/PPLs/Run VI.lvlibp">
				<Item Name="Clear Errors.vi" Type="VI" URL="../../builds/PPLs/Run VI.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Execute VI.lvclass" Type="LVClass" URL="../../builds/PPLs/Run VI.lvlibp/Project/Run VI/Execute VI/Execute VI.lvclass"/>
				<Item Name="File Exists - Scalar__ogtk.vi" Type="VI" URL="../../builds/PPLs/Run VI.lvlibp/1abvi3w/user.lib/_OpenG.lib/file/file.llb/File Exists - Scalar__ogtk.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="../../builds/PPLs/Run VI.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="../../builds/PPLs/Run VI.lvlibp/1abvi3w/vi.lib/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="../../builds/PPLs/Run VI.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="MGI Clear Error.vi" Type="VI" URL="../../builds/PPLs/Run VI.lvlibp/1abvi3w/user.lib/_MGI/Error Handling/MGI Clear Error.vi"/>
				<Item Name="MGI Gray if False.vi" Type="VI" URL="../../builds/PPLs/Run VI.lvlibp/1abvi3w/user.lib/_MGI/Application Control/MGI Gray if/MGI Gray if False.vi"/>
				<Item Name="MGI Open Explorer Window.vi" Type="VI" URL="../../builds/PPLs/Run VI.lvlibp/1abvi3w/user.lib/_MGI/File/MGI Open Explorer Window.vi"/>
				<Item Name="Run VI.lvclass" Type="LVClass" URL="../../builds/PPLs/Run VI.lvlibp/Project/Run VI/Run VI/Run VI.lvclass"/>
				<Item Name="System Exec.vi" Type="VI" URL="../../builds/PPLs/Run VI.lvlibp/1abvi3w/vi.lib/Platform/system.llb/System Exec.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="../../builds/PPLs/Run VI.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="../../builds/PPLs/Run VI.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Shell Command.lvlibp" Type="LVLibp" URL="../../builds/PPLs/Shell Command.lvlibp">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Config.ctl" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/Config.ctl"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Execute Shell Command.lvclass" Type="LVClass" URL="../../builds/PPLs/Shell Command.lvlibp/Execute Shell Command/Execute Shell Command.lvclass"/>
				<Item Name="Find Tag.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MGI Gray if True.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/user.lib/_MGI/Application Control/MGI Gray if/MGI Gray if True.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Shell Command.lvclass" Type="LVClass" URL="../../builds/PPLs/Shell Command.lvlibp/Shell Command.lvclass"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="../../builds/PPLs/Shell Command.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Solution.lvlibp" Type="LVLibp" URL="../../builds/PPLs/Solution.lvlibp">
				<Item Name="Interfaces" Type="Folder">
					<Item Name="Visualization.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Visualization/Visualization.lvclass"/>
					<Item Name="Build Artifact.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Build Artifact/Build Artifact.lvclass"/>
					<Item Name="Build Step.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Build Item/Build Step.lvclass"/>
					<Item Name="Item.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Item/Item.lvclass"/>
				</Item>
				<Item Name="Helpers" Type="Folder">
					<Item Name="Build Cache.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Build Cache/Build Cache.lvclass"/>
					<Item Name="Progress.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Progress/Progress.lvclass"/>
					<Item Name="Sub Solution.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Solution/Sub Solution/Sub Solution.lvclass"/>
					<Item Name="Build Aborter.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Build Aborter/Build Aborter.lvclass"/>
					<Item Name="XML Node.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/XML Reader/XML Node.lvclass"/>
					<Item Name="Installed Solution Items.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Installed Solution Items.vi"/>
					<Item Name="Node.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Tree Entry/Node.lvclass"/>
					<Item Name="Global.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Test Files/Tree/Global.vi"/>
					<Item Name="Unknown Item.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Unknown Item/Unknown Item.lvclass"/>
					<Item Name="Set Delegates.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Visualization/Set Delegates.vi"/>
					<Item Name="Lookup LabVIEW Info.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Item/Lookup LabVIEW Info.vi"/>
				</Item>
				<Item Name="Solution.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Solution/Solution.lvclass"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_XML.lvlib" Type="Library" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/xml/NI_XML.lvlib"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Load Plugin.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Load Plugin.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="Search 1D Array (String)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Search 1D Array (String)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (String)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (String)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Sort 1D Array (I32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (String)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Reorder 1D Array2 (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (String)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (String)__ogtk.vi"/>
				<Item Name="Filter 1D Array (String)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Filter 1D Array (String)__ogtk.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/system.llb/System Exec.vi"/>
				<Item Name="MGI Open Explorer Window.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/File/MGI Open Explorer Window.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="Trim Whitespace (String)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_OpenG.lib/string/string.llb/Trim Whitespace (String)__ogtk.vi"/>
				<Item Name="To Proper Case (String)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_OpenG.lib/string/string.llb/To Proper Case (String)__ogtk.vi"/>
				<Item Name="To Proper Case (String Array)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_OpenG.lib/string/string.llb/To Proper Case (String Array)__ogtk.vi"/>
				<Item Name="To Camel Case (String)__ogtk.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_OpenG.lib/string/string.llb/To Camel Case (String)__ogtk.vi"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="MGI Is Runtime.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Application Control/MGI Is Runtime.vi"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry View.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry View.ctl"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Query Registry Key Info.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Query Registry Key Info.vi"/>
				<Item Name="Enum Registry Keys.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Enum Registry Keys.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="MGI Create Directory Chain Behavior Enum.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/File/MGI Create Directory Chain Behavior Enum.ctl"/>
				<Item Name="MGI Create Directory Chain.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/File/MGI Create Directory Chain.vi"/>
				<Item Name="MGI Suppress Error Code (Scalar).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Error Handling/MGI Suppress Error Code/MGI Suppress Error Code (Scalar).vi"/>
				<Item Name="MGI RWA Options Cluster.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Options Cluster.ctl"/>
				<Item Name="MGI RWA Write Strings to File.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Write Strings to File.vi"/>
				<Item Name="MGI RWA Read Strings from File.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Read Strings from File.vi"/>
				<Item Name="MGI Scan From String (CDB).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Scan From String/MGI Scan From String (CDB).vi"/>
				<Item Name="MGI Scan From String (CSG).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Scan From String/MGI Scan From String (CSG).vi"/>
				<Item Name="MGI Scan From String (CXT).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Scan From String/MGI Scan From String (CXT).vi"/>
				<Item Name="MGI Scan From String (CXT[]).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Scan From String/MGI Scan From String (CXT[]).vi"/>
				<Item Name="MGI Scan From String (CSG[]).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Scan From String/MGI Scan From String (CSG[]).vi"/>
				<Item Name="MGI Scan From String (CDB[]).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Scan From String/MGI Scan From String (CDB[]).vi"/>
				<Item Name="MGI RWA Build Array Name.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Build Array Name.vi"/>
				<Item Name="MGI Insert Reserved Error.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Error Handling/MGI Insert Reserved Error.vi"/>
				<Item Name="MGI RWA Remove EOLs and Slashes.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Remove EOLs and Slashes.vi"/>
				<Item Name="MGI Windows Regional Ring.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Windows Get Regional String/MGI Windows Regional Ring.ctl"/>
				<Item Name="MGI Windows Get Regional String.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Windows Get Regional String.vi"/>
				<Item Name="MGI RWA Unreplace Characters.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Unreplace Characters.vi"/>
				<Item Name="MGI Get Cluster Elements.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Cluster/MGI Get Cluster Elements.vi"/>
				<Item Name="MGI RWA Convertion Direction Enum.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Convertion Direction Enum.ctl"/>
				<Item Name="MGI RWA Tag Lookup Cluster.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Tag Lookup Cluster.ctl"/>
				<Item Name="DWDT Empty Digital.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Waveform/DWDTOps.llb/DWDT Empty Digital.vi"/>
				<Item Name="DTbl Empty Digital.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Waveform/DTblOps.llb/DTbl Empty Digital.vi"/>
				<Item Name="MGI RWA Get Type Info.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Get Type Info.vi"/>
				<Item Name="MGI RWA Replace Characters.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Replace Characters.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="MGI RWA Build Line.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Build Line.vi"/>
				<Item Name="MGI U8 Data to Hex Str.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI U8 Data to Hex Str.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="MGI RWA Enque Top Level Data.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Enque Top Level Data.vi"/>
				<Item Name="MGI Hex Str to U8 Data.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Hex Str to U8 Data.vi"/>
				<Item Name="MGI RWA Handle Tag or Refnum.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Handle Tag or Refnum.vi"/>
				<Item Name="MGI RWA Process Array Elements.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Process Array Elements.vi"/>
				<Item Name="MGI RWA Anything to String.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Anything to String.vi"/>
				<Item Name="MGI Write Anything.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI Write Anything.vi"/>
				<Item Name="MGI RWA INI Tag Lookup.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA INI Tag Lookup.vi"/>
				<Item Name="MGI RWA Unprocess Array Elements.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Unprocess Array Elements.vi"/>
				<Item Name="MGI RWA String To Anything.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA String To Anything.vi"/>
				<Item Name="MGI Read Anything.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI Read Anything.vi"/>
				<Item Name="MGI Replace File Extension.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/File/MGI Replace File Extension.vi"/>
			</Item>
		</Item>
		<Item Name="Signer.dll" Type="Document" URL="../Signing Helpers/Signer/bin/Release/Signer.dll"/>
		<Item Name="Signer.lvlib" Type="Library" URL="../Signer.lvlib"/>
		<Item Name="Test UI.vi" Type="VI" URL="../../Test Files/Test UI.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Index 1D Array Elements (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (Boolean)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (CDB)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (CSG)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (CXT)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (DBL)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (EXT)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (I8)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (I16)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (I32)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (I64)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (LVObject)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (Path)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (SGL)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (String)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (U8)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (U16)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (U32)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (U64)__ogtk.vi"/>
				<Item Name="Index 1D Array Elements (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 1D Array Elements (Variant)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (Boolean)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (CDB)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (CSG)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (CXT)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (DBL)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (EXT)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (I8)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (I16)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (I32)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (I64)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (LVObject)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (Path)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (SGL)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (String)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (U8)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (U16)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (U32)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (U64)__ogtk.vi"/>
				<Item Name="Index 2D Array Elements (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index 2D Array Elements (Variant)__ogtk.vi"/>
				<Item Name="Index Array Elements__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Index Array Elements__ogtk.vi"/>
				<Item Name="MGI Caller&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Caller&apos;s VI Reference.vi"/>
				<Item Name="MGI Current VI&apos;s Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Current VI&apos;s Reference.vi"/>
				<Item Name="MGI Gray if Empty String.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Gray if/MGI Gray if Empty String.vi"/>
				<Item Name="MGI Gray if Error.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Gray if/MGI Gray if Error.vi"/>
				<Item Name="MGI Gray if False.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Gray if/MGI Gray if False.vi"/>
				<Item Name="MGI Gray if True.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Gray if/MGI Gray if True.vi"/>
				<Item Name="MGI Gray if Zero.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Gray if/MGI Gray if Zero.vi"/>
				<Item Name="MGI Gray if(PolyVI).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Gray if(PolyVI).vi"/>
				<Item Name="MGI Level&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Level&apos;s VI Reference.vi"/>
				<Item Name="MGI Origin at Top Left.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Origin at Top Left.vi"/>
				<Item Name="MGI Top Level VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Top Level VI Reference.vi"/>
				<Item Name="MGI VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="DOMUserDefRef.dll" Type="Document" URL="DOMUserDefRef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="mscorlib" Type="VI" URL="mscorlib">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Tree.lvclass" Type="LVClass" URL="../../Test Files/Tree/Tree.lvclass"/>
			<Item Name="user32" Type="VI" URL="user32">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Signer" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{48202095-E450-4442-9941-7BDC062F274B}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Signer</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/PPLs</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{E86B16E3-07AD-4065-B2DC-A5B0527AC9AC}</Property>
				<Property Name="Bld_version.build" Type="Int">59</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Signer.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/PPLs/Signer.lvlibp</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/PPLs</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{7D5691EE-541E-4D81-8B88-176713336A0F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Signer.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Signer.lvlib/Signer.lvclass/Private/Config.vi</Property>
				<Property Name="Source[2].properties[0].type" Type="Str">Window behavior</Property>
				<Property Name="Source[2].properties[0].value" Type="Str">Modal</Property>
				<Property Name="Source[2].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">MGI</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Signer</Property>
				<Property Name="TgtF_internalName" Type="Str">Signer</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 MGI</Property>
				<Property Name="TgtF_productName" Type="Str">Signer</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{E828FD80-5F9C-4241-912A-A236EE574EF1}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Signer.lvlibp</Property>
			</Item>
		</Item>
	</Item>
</Project>
